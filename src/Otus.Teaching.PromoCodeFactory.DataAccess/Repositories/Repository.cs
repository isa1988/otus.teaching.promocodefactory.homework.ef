﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : class, IEntity
    {
        protected DbContextDataAccess contextOptions;

        protected DbSet<T> dbSet { get; set; }

        public Repository(DbContextDataAccess contextOptions)
        {
            this.contextOptions = contextOptions;
        }

        public void Save()
        {
            contextOptions.SaveChanges();
        }

        public virtual async Task<T> Create(T entity)
        {
            var entry = await dbSet.AddAsync(entity);
        
            return entry.Entity;
        }

        public virtual async Task<IEnumerable<T>> GetAll()
        {
            return await dbSet.ToListAsync();
        }


        public virtual void Update(T entity)
        {
            dbSet.Update(entity);
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

    }

    public abstract class Repository<T, TId> : Repository<T>, IRepository<T, TId>
        where T : class, IEntity<TId>
        where TId : IEquatable<TId>
    {
        public Repository(DbContextDataAccess contextOptions) : base(contextOptions)
        {

        }

        public virtual async Task<T> GetById(TId id)
        {
            var query = await dbSet.FirstOrDefaultAsync(x => x.Id.Equals(id));
            return query;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Configuration
{
    class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Code).IsRequired();
            builder.Property(p => p.BeginDate).IsRequired();
            builder.Property(p => p.EndDate).IsRequired();
            builder.HasOne(p => p.PartnerCustomer)
                .WithMany(t => t.PromoCodes)
                .HasForeignKey(p => p.CustomerId);

            builder.HasOne(p => p.Preference)
                .WithMany(t => t.PromoCodes)
                .HasForeignKey(p => p.PreferenceId); 
        }
    }
}

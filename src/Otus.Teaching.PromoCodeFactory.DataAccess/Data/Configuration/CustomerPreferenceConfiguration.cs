﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Configuration
{
    class CustomerPreferenceConfiguration : IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder.HasKey(p => new { p.CustomerId, p.PreferenceId});
            builder.HasOne(p => p.Customer)
                .WithMany(t => t.CustomerPreferences)
                .HasForeignKey(p => p.CustomerId);

            builder.HasOne(p => p.Preference)
                .WithMany(t => t.CustomerPreferences)
                .HasForeignKey(p => p.PreferenceId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.DB;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataDbInitializer : IDbInitializer
    {
        public DataDbInitializer(IServiceScopeFactory serviceScopeProvider, ICustomerRepository customerRepository, ICustomerPreferenceRepository customerPreferenceRepository,
               IEmployeeRepository employeeRepository, IRoleRepository roleRepository, IPreferenceRepository preferenceRepository, IPromoCodeRepository promoCodeRepository)
        {
            this.serviceScopeProvider = serviceScopeProvider;
            this.customerRepository = customerRepository;
            this.customerPreferenceRepository = customerPreferenceRepository;
            this.employeeRepository = employeeRepository;
            this.roleRepository = roleRepository;
            this.preferenceRepository = preferenceRepository;
            this.promoCodeRepository = promoCodeRepository;
        }

        private IServiceScopeFactory serviceScopeProvider;
        private ICustomerRepository customerRepository;
        private ICustomerPreferenceRepository customerPreferenceRepository;
        private IEmployeeRepository employeeRepository;
        private IRoleRepository roleRepository;
        private IPreferenceRepository preferenceRepository;
        private IPromoCodeRepository promoCodeRepository;

        public async void Initialize()
        {
            using (var serviceScope = serviceScopeProvider.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<DbContextDataAccess>();
                context.Database.EnsureCreated();

                var prefence = new Preference()
                {
                    Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                    Name = "Театр",
                };
                await preferenceRepository.Create(prefence);
                prefence = new Preference()
                {
                    Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                    Name = "Семья",
                };
                await preferenceRepository.Create(prefence);
                prefence = new Preference()
                {
                    Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                    Name = "Дети",
                };
                await preferenceRepository.Create(prefence);
                preferenceRepository.Save();
                var role = new Role()
                {
                    Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                    Name = "Admin",
                    Description = "Администратор",
                };
                await roleRepository.Create(role);
                role = new Role()
                {
                    Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                    Name = "PartnerManager",
                    Description = "Партнерский менеджер"
                };
                await roleRepository.Create(role);
                roleRepository.Save();

                var employee = new Employee()
                {
                    Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                    Email = "owner@somemail.ru",
                    FirstName = "Иван",
                    LastName = "Сергеев",
                    RoleId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                    AppliedPromocodesCount = 5
                };
                await employeeRepository.Create(employee);
                employee = new Employee()
                {
                    Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                    Email = "andreev@somemail.ru",
                    FirstName = "Петр",
                    LastName = "Андреев",
                    RoleId = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                    AppliedPromocodesCount = 10
                };
                await employeeRepository.Create(employee);
                employeeRepository.Save();

                var customer = new Customer()
                    {
                        Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                    };
                await customerRepository.Create(customer);
                customerRepository.Save();

                var promoCode = new PromoCode()
                {
                    Id = Guid.NewGuid(),
                    PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                    CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                    BeginDate = DateTime.Now.Date,
                    EndDate = DateTime.Now.AddDays(14).Date,
                    Code = "0554554",
                    ServiceInfo = "что то"
                };
                await promoCodeRepository.Create(promoCode);
                promoCode = new PromoCode()
                {
                    Id = Guid.NewGuid(),
                    PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                    CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                    BeginDate = DateTime.Now.Date,
                    EndDate = DateTime.Now.AddDays(7).Date,
                    Code = "0554555",
                    ServiceInfo = "что то"
                };
                await promoCodeRepository.Create(promoCode);
                promoCodeRepository.Save();
            }
        }
    }
}


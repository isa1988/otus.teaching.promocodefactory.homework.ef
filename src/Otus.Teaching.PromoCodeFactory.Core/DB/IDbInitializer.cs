﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.DB
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}

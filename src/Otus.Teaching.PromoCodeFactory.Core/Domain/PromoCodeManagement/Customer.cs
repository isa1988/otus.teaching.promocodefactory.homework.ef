﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :IEntity<Guid>
    {
        public Customer()
        {
            PromoCodes = new List<PromoCode>();
            CustomerPreferences = new List<CustomerPreference>();
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public virtual List<PromoCode> PromoCodes { get; set; }

        public virtual List<CustomerPreference> CustomerPreferences { get; set; }
    }
}
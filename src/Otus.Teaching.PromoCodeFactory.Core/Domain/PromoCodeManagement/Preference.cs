﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }


        public virtual List<PromoCode> PromoCodes{ get; set; }
        public virtual List<CustomerPreference> CustomerPreferences { get; set; }
    }
}
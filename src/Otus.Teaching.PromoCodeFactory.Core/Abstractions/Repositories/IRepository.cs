﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : class, IEntity
    {
        Task<T> Create(T entity);
        Task<IEnumerable<T>> GetAll();
        void Update(T entity);
        void Delete(T entity);

        void Save();
    }

    public interface IRepository<T, TId> : IRepository<T>
        where T : class, IEntity<TId>
        where TId : IEquatable<TId>
    {
        Task<T> GetById(TId id);
    }
}